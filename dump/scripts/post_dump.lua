function handler(document)
	local document_field_names = { document:getFieldNames() }
	for i, field_name in ipairs(document_field_names) do
		if document:hasField(field_name) then
			field_value = document:getFieldValue(field_name)
			print ("POST:document:"..field_name.."=>"..field_value)
		end
	end
	local content = document:getContent()
	if content then 
		print ("POST:document:content=>"..content)
	else
		print ("POST:document:content=>nil")
	end
  	return true
end
