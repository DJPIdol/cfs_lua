function handler(document)

  -- constant so we can recognise this script
  local cfsLuaUnknown = "CFSLuaUnknownValue"

  -- this is where the IDX output will go
  local idx_out_folder = "/var/data/turkcell/idx/"
  local dredbname = "csv"
  local delimiter = ","
  local interest_columns = {["soru 1"]=true,["soru 2"]=true,
    ["soru 3"]=true,["soru 4"]=true,["anket adı"]=true,["Sonuç"]=true,
    ["Ana Başlık"]=true,["Kök Neden"]=true}
  local reference_column = "anket_no"
  local reference_index = -1

  -- for logging
  local config = get_config("cfs.cfg")
  local log = get_log(config, "ImportLogStream")

  -- check a file exists and IFF so return its value
  function getExistingFieldValue(field_name)
    if document:hasField (field_name) then
      return document:getFieldValue (field_name)
    else
      return CFSLuaUnknown
    end
  end

  -- wrote this fn because KV converts commas into tabs (OK) but then 
  -- consolidates tabs which destroys the column structure, so read the content
  -- direct from disk
  function getRawContent(filename)
    local f
    f = assert(io.open(filename, "r"))
    s = f:read("*all")
    f:close()
    return s
  end

  -- parse a CSV file into multiple subdocuments and create IDX for each
  function processCSV()
    -- local content = document:getContent()
    local content = getRawContent(getExistingFieldValue("DREFILENAME"))
    print (content)
    local column_headers = {}
    local line_count = -1

    -- iterate over the lines in the CSV
    for line in string.gmatch(content,"([^\n]-)[\n]") do
      -- ignore blank lines
      if #line > 0 then
        -- create a subdocument for rows
        line_count = line_count + 1
        local drecontent = ""
        local f
        local idx_out
        if line_count > 0 then
          idx_out = idx_out_folder..
            getExistingFieldValue("UUID").."_"..line_count..".idx"
          log:write_line(log_level_normal(),
            "Writing csv row to "..idx_out)
          f = assert(io.open(idx_out, "w"))
        end
        local column_index = 0

        --remove commas inbetween quotese, ceing with " - " so this does not
        -- break the column structure
        line_len = 0
        while #line ~= line_len do
          line_len = #line
          line = string.gsub(line,"\"([^\"]-),([^\"]-)\"","\"%1 - %2\"")
        end

        --interate across the columns
        for cell_value in string.gmatch(line..delimiter,"(.-)"..delimiter) do
          column_index = column_index + 1
          -- is this the header or a row?
          if line_count == 0 then
            -- it's a header, extract cell value as a field key
            column_headers[column_index]=cell_value
            if cell_value == reference_column then
              reference_index = column_index
              print ("reference index is ["..reference_index.."]")
            end
          else
            if column_index == reference_index then
              f:write("#DREREFERENCE anket_no:"..cell_value.."\n")
              f:write("#DREDBNAME "..dredbname.."\n")
              -- save path to IDX so can delete this in the second pass
              f:write("#DREFIELD IDX_OUT"..'="'..idx_out..'"'.."\n")
            end
            -- do we want this field?
            if interest_columns[column_headers[column_index]] == true and
               #cell_value > 0 then
              f:write("#DREFIELD "..
                string.gsub(column_headers[column_index],"[^a-zA-Z0-9]+","_")..
                '="'..cell_value..'"'.."\n")
              drecontent = drecontent.." "..cell_value
            end
          end
        end
        -- if this is a row then
        if line_count > 0 then
          f:write("#DRECONTENT\n")
          f:write(drecontent.."\n")
          f:write("#DREENDDOC\n")
          f:close()
        end
      end
    end
    -- do not index this because the child IDXs will be indexed instead
    return false
  end

  -- main starts here
  extn = getExistingFieldValue("ImportMagicExtension")
  if extn == "csv" then
    return processCSV()
  else
    --local path = get_filename(document)
    local path = getExistingFieldValue("IDX_OUT")
    print (path)
    extn = string.sub(path,#path-2,#path)
    if extn == "idx" then
      log:write_line( log_level_normal() ,
        "Read IDX now deleting ["..path.."]")
      -- delete the file
      os.remove(path)
      document:deleteField( "IDX_OUT", false )
      return true
    else
      log:write_line( log_level_warning() ,
        "Unexpected ["..extn.."] file, do not index")
      return false
    end
  end
end
