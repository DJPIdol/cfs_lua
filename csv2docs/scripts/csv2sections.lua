function handler(document)

	-- data preparation:
	-- 1. open *.xlsx file in e.g. excel
	-- 

	-- constant so we can recognise this script
	local cfsLuaUnknown = "CFSLuaUnknownValue"

	-- this is where the IDX output will go
	local idx_out_folder = "C:\\Program Files\\Autonomy\\IDOLServer\\data\\idx\\"
	local dredbname = "csv"
	--local delimiter = ","
	local delimiter = "	"
	
	--local interest_columns = {["anket_no"]=true, ["soru 4"]=true, ["Sonuç"]=true,
		--["Ana Başlık"]=true,["Kök Neden"]=true}
	local interest_columns = {["Ncst"]=true, ["Yorum"]=true, ["Sonuç"]=true,
		["Ana Başlık"]=true,["Kök Neden"]=true}
	
	local content_column = "Yorum"
	local reference_column = "Ncst"
	local clone_parent_column = "Ana Başlık"
	local clone_child_column = "Kök Neden"
	local clone_parent = ""
	local reference_index = -1
    local content_index = -1
	local clone_parent_index = -1
	local clone_child_index = -1

	-- for logging
	local config = get_config("CFS.cfg")
	local log = get_log(config, "ImportLogStream")

	-- check a file exists and IFF so return its value
	function getExistingFieldValue(field_name)
		if document:hasField (field_name) then
			return document:getFieldValue (field_name)
		else
			return CFSLuaUnknown
		end
	end

  	function getRawContent(filename)
		local f
		f = assert(io.open(filename, "r"))
		s = f:read("*all")
		f:close()
		return s
	end

	-- parse a CSV file into multiple subdocuments and create IDX for each
	function processCSV()
		-- local content = document:getContent()
		local content = getRawContent(getExistingFieldValue("DREFILENAME"))
		print (content)
		local column_headers = {}
		local line_count = -1

		-- iterate over the lines in the CSV
		for line in string.gmatch(content,"([^\n]-)[\n]") do
			-- ignore blank lines
			if #line > 0 then
				-- create a subdocument for rows
				line_count = line_count + 1
				local content = ""
				local f
				local idx_out
				if line_count > 0 then
					idx_out = idx_out_folder..
						getExistingFieldValue("UUID").."_"..line_count..".idx"
					log:write_line(log_level_normal(),
						"Writing csv row to "..idx_out)
					f = assert(io.open(idx_out, "w"))
				end
				local column_index = 0

				--remove commas inbetween quotes, they mess with everything
				line_len = 0
				while #line ~= line_len do
					line_len = #line
					line = string.gsub(line,"\"([^\"]-),([^\"]-)\"","\"%1 - %2\"")
				end

				--interate across the columns
				for cell_value in string.gmatch(line..delimiter,"(.-)"..delimiter) do
					column_index = column_index + 1
					-- is this the header or a row?
					if line_count == 0 then
						-- it's a header, extract cell value as a field key
						column_headers[column_index]=cell_value
						if cell_value == reference_column then
							reference_index = column_index
							log:write_line( log_level_normal() ,
                                "reference index is ["..reference_index.."]")
						elseif cell_value == content_column then
							content_index = column_index
							log:write_line( log_level_normal() ,
                                "content index is ["..content_index.."]")
						end
						if cell_value == clone_parent_column then
							clone_parent_index = column_index
							log:write_line( log_level_normal() ,
                                "clone parent index is ["..reference_index.."]")
						elseif cell_value == clone_child_column then
							clone_child_index = column_index
							log:write_line( log_level_normal() ,
                                "clone child index is ["..content_index.."]")
						end
						--column_headers[column_index]=string.gsub(cell_value
					else
						--print (column_index,column_headers[column_index],cell_value,interest_columns[column_headers[column_index]])
						if column_index == reference_index then
					  	    f:write("#DREREFERENCE "..reference_column..":"..cell_value.."\n")
				  		    f:write("#DREDBNAME "..dredbname.."\n")
							f:write("#DREFIELD IDX_OUT"..'="'..idx_out..'"'.."\n")
						elseif column_index == content_index then						
						    content = cell_value
						end
						
						-- 
						if column_index == clone_parent_index then
							clone_parent = cell_value
						elseif column_index == clone_child_index and 
							#cell_value == 0 then 
							cell_value = clone_parent_column..":"..clone_parent
						end
							
					    if interest_columns[column_headers[column_index]] == true and
						    #cell_value > 0 then
						    f:write("#DREFIELD "..
						        string.gsub(column_headers[column_index],"[^a-zA-Z0-9]+","_")..
	  						    '="'..cell_value..'"'.."\n")
						end
					end
				end
				-- if this is a row then
				if line_count > 0 then
					f:write("#DRECONTENT\n")
					f:write(content.."\n")
					f:write("#DREENDDOC\n")
					f:close()
				end
			end
		end
		-- do not index this because the child IDXs will be indexed instead
		return false
	end

	-- main starts here
	extn = getExistingFieldValue("ImportMagicExtension")
	--if extn == "csv" then
	if extn == "txt" then
		return processCSV()
	else
		--local path = get_filename(document)
		local path = getExistingFieldValue("IDX_OUT")
		print (path)
		extn = string.sub(path,#path-2,#path)
   		if extn == "idx" then
			log:write_line( log_level_normal() ,
			  "Read IDX deleting ["..path.."]")
			os.remove(path)
			local content = document:getContent()
			return #content > 0
		else
			log:write_line( log_level_warning() ,
  			"Unexpected ["..extn.."] file, do not index")
			return false
		end
	end
end
